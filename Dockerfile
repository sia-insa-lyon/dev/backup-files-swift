FROM alpine:3.12.0

RUN apk update \
        && apk upgrade \
        && apk add --no-cache \
        ca-certificates && apk add linux-headers libc-dev python3-dev gcc py3-pip py3-wheel && pip3 install python-swiftclient && pip3 install python-keystoneclient \
        && update-ca-certificates 2>/dev/null

WORKDIR /src/scripts
COPY backup.sh .

ENTRYPOINT ["sh"]
CMD ["./backup.sh"]
