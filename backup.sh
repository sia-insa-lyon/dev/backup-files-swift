#!/bin/sh

set -e
set -x

backup_FILE_NAME="${FILE_NAME}-$(date +"%Y-%m-%d_%Hh%M").backup.tar.gz"
echo "Creating tar archive"

tar -czvf ${backup_FILE_NAME} /data/

if [[ $? -ne 0 ]]; then
  echo "Back up not created, check logs"
  exit 1
fi

swift --os-project-name "${OS_PROJECT_NAME}" -V=3 --os-auth-url "${OS_AUTH_URL}" --os-storage-url "${OS_ENDPOINT}" upload "${OS_BUCKET_NAME}" "${backup_FILE_NAME}" --object-name "${backup_FILE_NAME}"
