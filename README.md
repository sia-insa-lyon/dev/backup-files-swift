# Backup S3

A container to interact between a Kubernetes cluster and a S3 system using swift to backup a file or directory.

## Helm values

| Value | Default | Comment |
|:------|---------|---------|
| image.registry | registry.gitlab.com/sia-insa-lyon/backup-files-swift ||
| image.tag | latest ||
| schedule | 0 1 * * * | The backup time of your database |
| fileName | "" | The name of your backup ("%Y-%m-%d_%Hh%M".backup.tar.gz" will be appended at the end of file name) |
| pvcName | "" | The script will backup everything in this volume |
| OS_PROJECT_NAME | "" | Name of project|
| OS_AUTH_URL | "" |Url of authentification |
| OS_USERNAME | "" | Name of user to use for backup |
| OS_PASSWORD | "" | Key of user |
| OS_ENDPOINT | "" | Url of endpoint (Bucket or BlockStorage) |
| OS_BUCKET_NAME | "" | Name of Bucket |
| nodeSelector.enable | false ||
| nodeSelector.label-key | "" | key of label for nodeSelector |
| nodeSelector.label-value | "" | value of label for nodeSelector|
